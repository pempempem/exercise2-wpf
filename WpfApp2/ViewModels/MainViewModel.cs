﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using WpfApp2.Commands;
using WpfApp2.DataAccess.Implementation;
using WpfApp2.DataAccess.Interface;
using WpfApp2.Models;

namespace WpfApp2.ViewModels
{
    public class MainViewModel
    {
        public ObservableCollection<Book> Books
        {
            get;
            set;
        }

        public Book NewBook
        {
            get;
            set;
        }
        public ICommand AddBook
        {
            get;
            private set;
        }

        private readonly IDialogService _dialogService;
        private readonly JsonBookReader _jsonBookReader;
        private readonly ISourceOperationStrategy _sourceOperations;

        public ICommand OpenFile
        {
            get;
            private set;
        }

        public ICommand SaveFile
        {
            get;
            private set;
        }

        public MainViewModel(IDialogService dialogService, ISourceOperationStrategy
        sourceOperations)
        {
            _dialogService = dialogService;
            _sourceOperations = sourceOperations;
            Books = new ObservableCollection<Book>();
            NewBook = new Book();
            AddBook = new RelayCommand(param => AddNewBook(), null);
            OpenFile = new RelayCommand(param => LoadDataFromFile(), null);
            SaveFile = new RelayCommand(param => SaveDataToFile(), null);
        }
        private void LoadDataFromFile()
        {
            var fileName = _dialogService.OpenFileDialog();
            if (fileName != null)
            {
                List<Book> bookList = _sourceOperations.GetReader(fileName).ReadBooks(fileName);
                Books.Clear();
                foreach (Book book in bookList)
                {
                    Books.Add(book);
                }
            }
        }

        private void SaveDataToFile()
        {
            var fileName = _dialogService.SaveFileDialog();
            if (fileName != null)
            {
                _sourceOperations.GetWriter(fileName).WriteBooks(fileName, Books.ToList());
            }
        }

        public MainViewModel()
        {
            Books = new ObservableCollection<Book>();
            NewBook = new Book();
            AddBook = new RelayCommand(param => AddNewBook(), null);
        }

        private void AddNewBook()
        {
            if (NewBook.IsValid)
            {
                Books.Add(new Book(NewBook));
            }
        }
    }
}
