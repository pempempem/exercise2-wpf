﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfApp2.Models;

namespace WpfApp2.DataAccess.Interface
{
    public interface ISourceWriter
    {
        void WriteBooks(string filePath, List<Book> books);
    }
}
