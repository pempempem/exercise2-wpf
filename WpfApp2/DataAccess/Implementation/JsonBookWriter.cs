﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfApp2.DataAccess.Interface;
using WpfApp2.Models;

namespace WpfApp2.DataAccess.Implementation
{
    class JsonBookWriter : ISourceWriter
    {
        public void WriteBooks(string filePath, List<Book> books)
        {
            File.WriteAllText(filePath,JsonConvert.SerializeObject(books.ToArray()));
        }
    }
}
