﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using WpfApp2.DataAccess.Interface;
using WpfApp2.Models;

namespace WpfApp2.DataAccess.Implementation
{
    class XmlBookWriter : ISourceWriter
    {
        static XmlSerializer serializer = new XmlSerializer(typeof(List<Book>), new XmlRootAttribute("BookList"));

        public void WriteBooks(string filePath, List<Book> books)
        {
            using (var writer = new StreamWriter(filePath, true))
            {
                serializer.Serialize(writer, books);
                writer.Close();
            }
        }
    }
}
