﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfApp2.DataAccess.Interface;
using WpfApp2.Models;

namespace WpfApp2.DataAccess.Implementation
{
    public class JsonBookReader : ISourceReader
    {
        public List<Book> ReadBooks(string path)
        {
            string books = File.ReadAllText(path);
            List<Book> BookList = JsonConvert.DeserializeObject<List<Book>>(books);
            return BookList;
        }
    }
}
