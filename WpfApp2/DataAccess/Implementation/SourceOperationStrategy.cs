﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfApp2.DataAccess.Interface;

namespace WpfApp2.DataAccess.Implementation
{
    public class SourceOperationStrategy : ISourceOperationStrategy
    {
        private static readonly Dictionary<string, ISourceReader> _readers;
        private static readonly Dictionary<string, ISourceWriter> _writers;

        public ISourceWriter GetWriter(string fileName)
        {
            var extension = Path.GetExtension(fileName);
            if (_writers.TryGetValue(extension, out ISourceWriter writer))
                return writer;
            return null;
        }

        public ISourceReader GetReader(string fileName)
        {
            var extension = Path.GetExtension(fileName);
            if (_readers.TryGetValue(extension, out ISourceReader reader))
                return reader;
            return null;
        }

        static SourceOperationStrategy()
        {
            _readers = new Dictionary<string, ISourceReader>();
            _readers.Add(".xml", new XmlBookReader());
            _readers.Add(".json", new JsonBookReader());

            _writers = new Dictionary<string, ISourceWriter>();
            _writers.Add(".xml", new XmlBookWriter());
            _writers.Add(".json", new JsonBookWriter());
        }  
    }
}
